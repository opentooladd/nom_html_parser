coverage: install-tarpaulin
	cargo tarpaulin -v --out Html Lcov --output-dir coverage/

build:
	cargo build

install-tarpaulin: ~/.cargo/bin/cargo-tarpaulin

# see deps here: https://github.com/xd009642/tarpaulin
~/.cargo/bin/cargo-tarpaulin:
	cargo install cargo-tarpaulin
