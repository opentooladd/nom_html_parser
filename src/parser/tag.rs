use crate::parser::attributes::*;
use crate::parser::utils::*;

type HtmlTag = String;

use nom::{
    IResult,
    error::{
        VerboseError,
    }
};

#[derive(Debug, PartialEq, Clone)]
pub struct HtmlElement {
    pub tag: HtmlTag,
    pub attributes: TagAttributes,
    pub nodes: Vec<Node>,
}

pub fn html_tag() -> impl Fn(&str) -> IResult<&str, HtmlElement, VerboseError<&str>> {
    move |input| {
        // println!("into child: input: {:?}", input);
        let (input, (space_before, tag, attributes, _closing_char)) = space_and_opening_tag(input)?;
        // println!("tag: {:?}", tag);
        let ending_tag = format!("\n{}</{}>", space_before, tag);
        // println!("child ending tag: {:?}", ending_tag);
        let (input, (inner_content, _end)) = parse_inner_content(ending_tag)(input)?;
        // println!("child inner_content: {:?}", inner_content);
        // let (input, maybe_closing_tag) = opt!(input, tag!(ending_tag.as_ref()))?;
        // println!("child maybe closing tag: {:?}", maybe_closing_tag);

        let (_, nodes) = content_and_childs(inner_content)?;
        // println!("child nodes: {:?}", nodes);
        //

        Ok((input, HtmlElement {
            tag: tag.to_string(),
            attributes,
            nodes,
        }))
    }
}

pub fn html(input: &str) -> IResult<&str, HtmlElement, VerboseError<&str>> {
    // println!("first input: {:?}", input);

    // trim starting newlines
    let input = input.trim_start_matches('\n');
    let input =input.trim_end();
    let input =input.trim_end_matches('\n');

    // println!("after trimming: {:?}", input);

    let (input, element) = html_tag()(input)?; 
    
    let (input, rest) = nom::character::complete::multispace0(input)?;
    println!("rest of data after ending tag: {:?}", rest);
    println!("after rest: {:?}", input);

    Ok((input, HtmlElement {
        tag: element.tag,
        attributes: element.attributes,
        nodes: element.nodes,
    }))
}

#[test]
fn parse_simple_tags_test() -> Result<(), String> {
    let element = html(r#"
        <>
        </>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "".to_string(),
       attributes: vec![],
       nodes: vec![],
    }); 
    Ok(())
}

#[test]
fn parse_simple_tags_and_attributes_test() -> Result<(), String> {
    let element = html(r#"
        <div class=`test` @test=`test` data-test=`test`>
        </div>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "div".to_string(),
       attributes: vec![
           (Attribute::Class("class".to_string()), "test".to_string()),
           (Attribute::Event("test".to_string()), "test".to_string()),
           (Attribute::Custom("data-test".to_string()), "test".to_string()),
       ],
       nodes: vec![],
    }); 
    Ok(())
}

#[test]
fn parse_tag_and_childs_test() -> Result<(), String> {
    let element = html(r#"
        <>
            <>
            </>
        </>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "".to_string(),
       attributes: vec![],
       nodes: vec![
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![],
        })
       ],
    });
    Ok(())
}

#[test]
fn parse_tag_and_multiple_childs_test() -> Result<(), String> {
    let element = html(r#"
        <>
            <>
            </>
            <>
            </>
        </>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "".to_string(),
       attributes: vec![],
       nodes: vec![
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![],
        }),
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![],
        }),
       ],
    });
    Ok(())
}

#[test]
fn parse_tag_and_multiple_childs_and_inner_childs_test() -> Result<(), String> {
    let element = html(r#"
        <>
            <>
                <>
                </>
            </>
            <>
            </>
        </>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "".to_string(),
       attributes: vec![],
       nodes: vec![
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![
                Node::Element(HtmlElement {
                   tag: "".to_string(),
                   attributes: vec![],
                   nodes: vec![],
                }),
           ],
        }),
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![],
        }),
       ],
    });
    Ok(())
}

#[test]
fn parse_tag_and_two_childs_and_multiple_inner_childs_test() -> Result<(), String> {
    let element = html(r#"
        <>
            <>
                <>
                    <>
                    </>
                </>
                <>
                </>
            </>
            <>
            </>
        </>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "".to_string(),
       attributes: vec![],
       nodes: vec![
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![
                Node::Element(HtmlElement {
                   tag: "".to_string(),
                   attributes: vec![],
                   nodes: vec![
                    Node::Element(HtmlElement {
                       tag: "".to_string(),
                       attributes: vec![],
                       nodes: vec![],
                    }),
                   ],
                }),
                Node::Element(HtmlElement {
                   tag: "".to_string(),
                   attributes: vec![],
                   nodes: vec![],
                }),
           ],
        }),
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![],
        }),
       ],
    });
    Ok(())
}

#[test]
fn parse_tag_and_multiples_childs_and_multiple_inner_childs_test() -> Result<(), String> {
    let element = html(r#"
        <>
            <>
                <>
                    <>
                    </>
                </>
                <>
                </>
            </>
            <>
            </>
            <>
                <>
                    <>
                    </>
                </>
                <>
                </>
            </>
        </>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "".to_string(),
       attributes: vec![],
       nodes: vec![
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![
                Node::Element(HtmlElement {
                   tag: "".to_string(),
                   attributes: vec![],
                   nodes: vec![
                    Node::Element(HtmlElement {
                       tag: "".to_string(),
                       attributes: vec![],
                       nodes: vec![],
                    }),
                   ],
                }),
                Node::Element(HtmlElement {
                   tag: "".to_string(),
                   attributes: vec![],
                   nodes: vec![],
                }),
           ],
        }),
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![],
        }),
        Node::Element(HtmlElement {
           tag: "".to_string(),
           attributes: vec![],
           nodes: vec![
                Node::Element(HtmlElement {
                   tag: "".to_string(),
                   attributes: vec![],
                   nodes: vec![
                    Node::Element(HtmlElement {
                       tag: "".to_string(),
                       attributes: vec![],
                       nodes: vec![],
                    }),
                   ],
                }),
                Node::Element(HtmlElement {
                   tag: "".to_string(),
                   attributes: vec![],
                   nodes: vec![],
                }),
           ],
        }),
       ],
    });
    Ok(())
}

#[test]
fn parse_tag_and_content_test() -> Result<(), String> {
    let element = html(r#"
        <>
            <>
                test child content
            </>
            test me i'am content
        </>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "".to_string(),
       attributes: vec![],
       nodes: vec![
           Node::Element(HtmlElement {
            tag: "".to_string(),
            attributes: vec![],
            nodes: vec![
                Node::Text("                test child content".to_string())
            ],
           }),
           Node::Text("            test me i'am content".to_string()),
       ],
    }); 
    Ok(())
}

#[test]
fn parse_tag_and_content_nested_test() -> Result<(), String> {
let element = html(r#"
        <>
            <>
                <>
                test content nested
                again
                </>
                test child content
            </>
            test me i'am content
            test other content
        </>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "".to_string(),
       attributes: vec![],
       nodes: vec![
           Node::Element(HtmlElement {
            tag: "".to_string(),
            attributes: vec![],
            nodes: vec![
                Node::Element(HtmlElement {
                    tag: "".to_string(),
                    attributes: vec![],
                    nodes: vec![
                        Node::Text("                test content nested
                again".to_string()),
                    ],
               }),
               Node::Text("                test child content".to_string()),
            ],
           }),
           Node::Text("            test me i'am content
            test other content".to_string()),
       ],
    }); 
    Ok(())
}

#[test]
fn empty_input_test() -> Result<(), String> {
    let input = r#""#;
    let element = html(input);
    Ok(())
}

#[test]
fn parse_tag_error_test() -> Result<(), String> {
    use nom::error::VerboseErrorKind;
    use crate::parser::errors::CLOSING_TAG_ERROR;

    let input = r#"<>
        <>"#;
    let element = html(input);
    match element.err().unwrap() {
        nom::Err::Error(e) => {
            assert_eq!(e.errors[1], ("\n        <>", VerboseErrorKind::Context(CLOSING_TAG_ERROR)));
            Ok(())
        }
        _ => Err("failed".to_string())
    }
}

#[test]
fn component_attribute_test() -> Result<(), String> {
    let input = r#"
        <div component=`TestComponent`>
        </div>
        "#;
    let element = html(input).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "div".to_string(),
       attributes: vec![
           (Attribute::Component("component".to_string()), "TestComponent".to_string()),
       ],
       nodes: vec![],
    });
    Ok(())
}

#[test]
fn component_test() -> Result<(), String> {
    let input = r#"
        <div>
            <@div>
            </div>
        </div>
        "#;
    let element = html(input).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "div".to_string(),
       attributes: vec![],
       nodes: vec![
           Node::Component(
               HtmlElement {
                   tag: "div".to_string(),
                   attributes: vec![],
                   nodes: vec![],
               }
            ),
       ],
    });
    Ok(())
}

#[test]
fn complex_content_test() -> Result<(), String> {
    let element = html(r#"
        <>
            {{#state.coins}}
                <li>
                    {{.}}
                </li>
            {{/state.coins}}
        </>
        "#).map_err(|e| e.to_string())?;
    assert_eq!(element.1, HtmlElement {
       tag: "".to_string(),
       attributes: vec![],
       nodes: vec![
           Node::Text(r#"            {{#state.coins}}
                <li>
                    {{.}}
                </li>
            {{/state.coins}}"#.to_string())
       ],
    }); 
    Ok(())
}
