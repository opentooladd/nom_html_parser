pub const IDENTIFIER_ERROR: &'static str  = "failed to parse identifier. \nNeeds # or . as first character for the moment.";
pub const NO_OPENING_BRACE_ERROR: &'static str  = "failed to parse opening brace.";
pub const NO_CLOSING_BRACE_ERROR: &'static str  = "failed to parse closing brace.";
