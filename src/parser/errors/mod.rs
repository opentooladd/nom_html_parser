// generic errors
pub const NEW_LINE_ERROR: &'static str  = "failed to parse new line.\n For the moment, new line are required to parse content correctly. Separate you elements correctly.";

// html tag errors
pub const TAG_CONTENT_ERROR: &'static str = "failed to parse html tag content.";
pub const CLOSING_TAG_ERROR: &'static str  = "failed to find matching closing tag";

// Attribute errors
pub const ATTRIBUTE_VALUE_ERROR: &'static str  = "failed to parse html attribute value";
pub const ATTRIBUTE_TAG_ERROR: &'static str  = "failed to parase html attribute tag. a valid tag passed \nthe function |s| s.is_alphanumeric() || s == '_' || s == '-' || s == '@' || s == '{' || s == '}' || s == '\'' and expect\n a space at the end";
pub const ATTRIBUTE_SPACE_BEFORE_ERROR: &'static str  = "failed to parse html attribute content";
pub const ATTRIBUTE_MISSING_EQUAL_ERROR: &'static str  = "missing = sign in html attribute";
pub const ATTRIBUTE_MISSING_OPENING_QUOTE_ERROR: &'static str  = "missing opening quote in html attribute value";
pub const ATTRIBUTE_MISSING_CLOSING_QUOTE_ERROR: &'static str  = "missing closing quote in html attribute value";
